const margin = 40;
const arrow = 10;
let board;
const incCount = n => n ? n + incCount(n-1) : n;
class Board{
    constructor(){
        this.graphs = [];
        createCanvas(window.innerWidth, window.innerHeight);
        background(80, 80);
        strokeWeight(4);
        fill(0);
        line(margin, height - margin, margin, margin);
        line(width - margin, height - margin, margin, height - margin);
        line(margin, margin, margin + arrow, margin + arrow);
        line(margin, margin, margin - arrow, margin + arrow);
        line(width - margin, height - margin, width - margin - arrow, height - margin + arrow);
        line(width - margin, height - margin, width - margin - arrow, height - margin - arrow);
        textSize(margin * 2 / 3);
        text(0, arrow, height - arrow);
        text('y', arrow, margin - arrow);
        text('x', width - margin + arrow, height - arrow);
        this.container = createDiv('');
        this.container.position(margin*2, height - margin*2);
        this.addNewGraphButton();
        this.current_duration = createElement('span', '');
        this.current_duration.position(width-2*margin, 2*margin);
        this.current_duration.style('font-size', '45px');
    }
    addNewGraphButton(){
        let addFormContainer = createDiv('');
        let labelDuration = createElement('label', 'Duration:');
        labelDuration.parent(addFormContainer);
        let duration = createInput();
        duration.parent(addFormContainer);
        let labelStart = createElement('label', 'Start:');
        labelStart.parent(addFormContainer);
        let startPoint = createInput();
        startPoint.parent(addFormContainer);
        let labelCoef = createElement('label', 'Coef:');
        labelCoef.parent(addFormContainer);
        let incCoef = createInput();
        incCoef.parent(addFormContainer);
        let button = createButton('submit');
        button.parent(addFormContainer);
        const createGraph = () => {
            let graph = new DrawGraph({ start: startPoint.value(), duration: duration.value(), inc: incCoef.value() });
            graph.addParent(this.container);
            this.graphs.push(graph);
        }
        button.mousePressed(createGraph);
        addFormContainer.parent(this.container);
    }
    showSpans(x){
        for (let i=0; i<this.graphs.length; i++){
            this.graphs[i].showSpan(x);
            this.current_duration.html(this.graphs[i].current_duration);
        }
    }
}
class DrawGraph{
    constructor(opts){
        this.start = opts.start;
        this.inc = opts.inc;
        this.duration = opts.duration;
        this.draw();
        this.span = createElement('span', ' ');
        this.span.style('background-color', this.convertHEX());
    }
    draw() {    
        this.rgb = [floor(random(0, 256)), floor(random(0, 256)), floor(random(0, 256))];
        stroke(this.rgb[0], this.rgb[1], this.rgb[2]);
        strokeWeight(5);
        line(margin, height - margin - this.start, width - margin, height - margin - this.start-this.inc*this.duration);
        textSize(margin/2);
        noStroke();
        text(this.start, 0, height - margin - this.start);
    }
    convertHEX() {
        let hex1 = this.rgb[0] > 16 ? this.rgb[0].toString(16) : '0' + this.rgb[0].toString(16);
        let hex2 = this.rgb[1] > 16 ? this.rgb[1].toString(16) : '0' + this.rgb[1].toString(16);
        let hex3 = this.rgb[2] > 16 ? this.rgb[2].toString(16) : '0' + this.rgb[2].toString(16);
        let hex = '#' + hex1 + hex2 + hex3;
        return hex;
    }
    showSpan(x) {
        this.current_duration = floor(x / (width - 2 * margin) * this.duration);
        this.span.html(this.start * this.current_duration + incCount(this.current_duration) * this.inc);
    }
    addParent(div){
        this.span.parent(div);
    }
}

function setup(){
    board = new Board();
}

function draw(){

}

function mousePressed(){
    board.showSpans(mouseX);
}
